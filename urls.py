from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', redirect_to, {'url': '/chatlogs/post'}),
    url(r'^chatlogs/post/', 'chatlogs.views.post_msg', name='post_msg'),
    url(r'^chatlogs/get/', 'chatlogs.views.get_msg', name='get_msg'),
    url(r'^chatlogs/delete/(?P<path>.*)', 'chatlogs.views.delete_msg', name='delete_msg'),
    url(r'^profile/', 'chatlogs.views.profile', name='profile'),
    url(r'^accounts/profile/', redirect_to, {'url': '/'}),
    url(r'^signup/', 'chatlogs.views.signup', name='signup'),
    url(r'^accounts/login/', 'django.contrib.auth.views.login', name='authentication'),
    url(r'^accounts/logout/', 'chatlogs.views.logout_view', name='logout'),
          

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
#     (r'static/(?P<path>.*)$'), 'django.views.static.serve', {'document_root':settings.STATIC_ROOT}
)
