from django.contrib import admin
from chatlogs.models import *

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['id','user','department','date_of_birth','sex_type']
    search_fields = ['id','user__username']
    list_filter = ['department']
admin.site.register(UserProfile,UserProfileAdmin)

class MessageAdmin(admin.ModelAdmin):
    inline = [User]
    list_display = ['id','user','created_at','is_approved']
    search_fields = ['id','user__username','is_approved']
    list_filter = ['created_at','is_approved']
    filter_horizontal = ('members',)

admin.site.register(Message,MessageAdmin)
