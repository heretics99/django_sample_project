from django.db import models
from django.contrib.contenttypes.models import ContentType
from datetime import datetime
from django.db.models.fields.related import ForeignKey
from django.contrib.contenttypes import generic
from django.contrib.auth.models import User
from django.conf import settings

DEPARTMENTS= (
        ('Ungrouped','Ungrouped'),
        ('HR','Human Resources'),
        ('BK','Backend Engineers'),
        ('UI','User Interface'),
        ('GR','Graphics Engineers'),
        ('HEN','Hardware Engineers'),
        ('QA','Quality Assurance'),
        ('OTH','Other Dept Guys'),
)

SEX= (
      ('M', 'Male'),
      ('F', 'Female')
)
class Department(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=50)
    def __unicode__(self):
        return u"%s"%self.name
    class Meta:
        db_table = "db_departments"
    
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    department = models.ForeignKey(Department, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    sex_type = models.CharField(max_length=10, choices=SEX, null=True, blank=True)
    class Meta:
        db_table = 'db_user_profiles'
    def __unicode__(self):
        return u"%s"%(self.user)

class Message(models.Model) :
    body = models.TextField(blank=True,null=True)
    user= models.ForeignKey(User)
    created_at = models.DateTimeField(blank=True, default = datetime.now)
    updated_at = models.DateTimeField(default = datetime.now, blank=True)
    is_approved = models.PositiveSmallIntegerField(default = 0)
    members = models.ManyToManyField(User, related_name="messagem2m_set")
    def __unicode__ (self) :
            return u"%s"%(self.title)
    class Meta:
            db_table='db_message'
                
