# Create your views here.
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.context_processors import csrf
from chatlogs.models import *


@login_required
def post_msg(request):
    """ Its the home view. Handles the composed message. Returns confirmation. Login is required."""
    data = {}
    limit = request.GET.get('limit', 10)
    start = request.GET.get('start', 0)
    data['messages'] = Message.objects.filter(members = request.user).order_by('-created_at')[start:limit]
    data['departments'] = Department.objects.all()
    if request.method == 'POST':
        data['status'] = ""
        usernames = request.POST.getlist('usernames')
        message = request.POST.get('message')
        if len(usernames)>0:
            if len(message)>0:
                msg_obj = Message(body=message, user=request.user, is_approved=1)
                msg_obj.save()
                msg_obj.members.add(*[int(x) for x in usernames])
                data['status'] = "Your Message is posted Successfully."
            else:
                data['status'] = "Message is missing."
        else:
            data['status'] = "You need to put atleast one receipent before you can post."
        data['body'] = message
    return render_to_response('post_msg.html',{'data':data}, RequestContext(request))

@login_required
def get_msg(request):
    """ Inbox view. Shows all messages sent to user."""
    data = {}
    try:
        limit = int(request.GET.get('limit'))
    except:
        limit = 10
    try:
        start = int(request.GET.get('start'))
    except:
        start = 0
    if request.method == 'POST':
        todelete = request.POST.getlist('todelete')
        for m in todelete:
            Message.objects.get(pk = int(m)).members.remove(request.user)
        data['status'] = "success"
    data['messages'] = Message.objects.filter(members = request.user).order_by('-created_at')[start:limit]
    return render_to_response('get_msg.html',{'data':data}, RequestContext(request))

@login_required
def delete_msg(request, msg_id):
    """ Inbox view. Shows all messages sent to user."""
    data = {}
    data['messages'] = Message.objects.filter(members = request.user)
    return render_to_response('get_msg.html',{'data':data}, RequestContext(request))

@login_required
def profile(request):
    """ Option to change user profile."""
    data = {}
    data['username'] = request.user.username
    data['departments'] = Department.objects.all()
    if request.method == 'POST':
        try:
            cu = UserProfile.objects.get(user=request.user)
        except:
            cu = UserProfile(user=request.user)
        new_department = request.POST.get('department')
        cu.department = Department.objects.get(id=int(new_department))
        cu.save()
        new_password = request.POST.get('password')
        print new_department, new_password
        if(len(new_password)>0):
            u = User.objects.get(pk=request.user.id)
            u.set_password(new_password)
            u.save() 
        data['status'] = "success"

    try:
        data['current_department'] = UserProfile.objects.get(user=request.user).department.id
    except:
        data['current_department'] = ""
    return render_to_response('profile.html',{'data':data}, RequestContext(request))

def logout_view(request):
    """Log users out and re-direct them to the main page."""
    logout(request)
    return HttpResponseRedirect('/')


def signup(request):
    data = {}
    data['departments'] = Department.objects.all()
    if request.method == 'POST':
        username = request.POST.get('username')
        department = request.POST.get('department')
        password = request.POST.get('password')
        password_repeat = request.POST.get('password_repeat')
        if len(username) > 0 and len(password) > 0:
            if password_repeat == password:
                try:
                    user = User.objects.create_user(username, email="", password=password)
                    try:
                        cu = UserProfile(user=user, department_id = department)
                        cu.save()
                        auth_user = authenticate(username=username, password=password)
                        if auth_user is not None:
                            login(request, auth_user)
                        return HttpResponseRedirect('/')
                    except Exception as e:
                        data['status'] = "User Profile not created"
                except:
                    data['status'] = "You're Late, that Username is already taken!"
            else:
                  data['status'] = "Easy, your passwords dont match."
        else:
            data['status'] = "Well, you cant keep your username or password blank, because we cant read your mind (yet)."
    return render_to_response('registration/registration.html', {'data':data}, RequestContext(request))
